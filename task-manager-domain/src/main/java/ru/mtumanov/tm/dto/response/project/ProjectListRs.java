package ru.mtumanov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRs extends AbstractResultRs {

    @Nullable
    private List<Project> projects;

    public ProjectListRs(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

    public ProjectListRs(@NotNull final Throwable err) {
        super(err);
    }

}