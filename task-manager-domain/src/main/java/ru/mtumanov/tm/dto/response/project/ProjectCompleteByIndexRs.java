package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public class ProjectCompleteByIndexRs extends AbstractProjectRs {

    public ProjectCompleteByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectCompleteByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
