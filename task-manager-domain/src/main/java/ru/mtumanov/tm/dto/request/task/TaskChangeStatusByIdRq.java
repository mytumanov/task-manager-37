package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;
import ru.mtumanov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRq(@Nullable final String token, @Nullable final String id, @Nullable final Status status) {
        super(token);
        this.id = id;
        this.status = status;
    }

}