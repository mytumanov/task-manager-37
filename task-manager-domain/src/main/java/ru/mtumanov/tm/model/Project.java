package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.model.IWBS;
import ru.mtumanov.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ID:" + id + " " +
                "NAME:" + name + " " +
                "DESCRIPTION:" + description + " " +
                "STATUS:" + getStatus().getDisplayName() + " " +
                "USER ID: " + getUserId() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Project)) {
            return false;
        }
        Project project = (Project) o;
        return Objects.equals(name, project.name)
                && Objects.equals(description, project.description)
                && Objects.equals(status, project.status)
                && Objects.equals(created, project.created)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}