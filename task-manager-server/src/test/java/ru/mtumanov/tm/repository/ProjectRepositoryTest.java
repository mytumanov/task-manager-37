package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.service.ConnectionService;
import ru.mtumanov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project name: " + i);
            project.setDescription("Project description: " + i);
            if (i <= 5)
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @After
    public void clearRepository() throws Exception {
        projectRepository.clear();
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Project project = new Project();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER_ID_1);

        projectList.add(project);
        projectRepository.add(USER_ID_1, project);
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testClear() throws Exception {
        assertNotEquals(0, projectRepository.getSize());
        projectRepository.clear();
        assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testExistById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectRepository.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertNotEquals(projectsUser1, projectsUser2);

        @NotNull final List<Project> actualProjectsUser1 = projectRepository.findAll(USER_ID_1);
        @NotNull final List<Project> actualProjectsUser2 = projectRepository.findAll(USER_ID_2);
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(projectsUser2, actualProjectsUser2);
    }

    @Test
    public void testFindOneById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionFindOneById() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testGetSize() throws Exception {
        assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testRemove() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.remove(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemove() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.remove(USER_ID_1, new Project());
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemoveById() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.removeById(USER_ID_2, UUID.randomUUID().toString());
        }
    }

}
