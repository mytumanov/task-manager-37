package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.response.project.*;
import ru.mtumanov.tm.enumerated.ProjectSort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Session;

import javax.annotation.Nullable;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdRs projectChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String projectId = request.getId();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = session.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, projectId, status);
            return new ProjectChangeStatusByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectClearRs projectClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            getServiceLocator().getProjectService().clear(userId);
            return new ProjectClearRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectClearRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCompleteByIdRs projectCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
            return new ProjectCompleteByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCreateRs projectCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Project project = getServiceLocator().getProjectService().create(userId, name, description);
            return new ProjectCreateRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCreateRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectListRs projectList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final ProjectSort sort = request.getSort();
            @Nullable Comparator<Project> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull List<Project> projects = getServiceLocator().getProjectService().findAll(userId, comparator);
            return new ProjectListRs(projects);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectListRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIdRs projectRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Project project = getServiceLocator().getProjectService().removeById(userId, id);
            return new ProjectRemoveByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectShowByIdRs projectShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Project project = getServiceLocator().getProjectService().findOneById(userId, id);
            return new ProjectShowByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectStartByIdRs projectStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
            return new ProjectStartByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIdRs projectUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRq request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
            return new ProjectUpdateByIdRs(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIdRs(e);
        }
    }

}
