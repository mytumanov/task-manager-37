package ru.mtumanov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.ISessionRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.model.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;

public class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

    @Getter
    private final String tableName = "tm_session";

    public SessionRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @Override
    @NotNull
    public Session add(@NotNull final String userId, @NotNull final Session model) throws SQLException {
        @NotNull final String sql = String.format("INSERT INTO %s (id, user_id, created, role) VALUES(?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.setTimestamp(3, new Timestamp(model.getCreated().getTime()));
            statement.setString(4, model.getRole().toString());
            statement.executeUpdate();
            connection.commit();
        }
        return null;
    }

    @Override
    @NotNull
    public Session add(@NotNull final Session model) throws SQLException {
        add(model.getUserId(), model);
        return model;
    }

    @Override
    @NotNull
    public Collection<Session> add(@NotNull final Collection<Session> models) throws SQLException {
        for (Session user : models) {
            add(user);
        }
        return models;
    }

    @Override
    @NotNull
    public Session update(@NotNull final String id, @NotNull final Session model) throws SQLException {
        @NotNull final String sql = String.format("UPDATE %s SET role = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getRole().toString());
            statement.setString(2, id);
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Override
    @NotNull
    protected Session fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setCreated(row.getTimestamp("created"));
        session.setRole(Role.valueOf(row.getString("role")));
        session.setUserId(row.getString("user_id"));
        return session;
    }

}
