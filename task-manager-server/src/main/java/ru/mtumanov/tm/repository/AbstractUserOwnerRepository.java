package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IUserOwnedRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    protected AbstractUserOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public abstract M add(@NotNull final String userId, @NotNull final M model) throws SQLException;

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
            connection.commit();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try {
            findOneById(userId, id);
            return true;
        } catch (AbstractEntityNotFoundException e) {
            return false;
        }
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                while (result.next()) resultList.add(fetch(result));
            }
        }
        return resultList;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? ORDER BY %s", getTableName(), getComporator(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                while (result.next()) resultList.add(fetch(result));
            }
        }
        return resultList;
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id)
            throws AbstractEntityNotFoundException, SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                if (!result.next()) throw new EntityNotFoundException();
                return fetch(result);
            }
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                result.next();
                return result.getInt("count");
            }
        }
    }

    @Override
    @NotNull
    public M remove(@NotNull final String userId, @NotNull final M model) throws AbstractEntityNotFoundException, SQLException {
        return removeById(userId, model.getId());
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractEntityNotFoundException, SQLException {
        @NotNull final M model = findOneById(userId, id);
        return remove(model);
    }

}
