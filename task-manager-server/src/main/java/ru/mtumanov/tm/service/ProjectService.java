package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.model.Project;

import java.sql.SQLException;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    @NotNull
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.update(id, project);
        return project;
    }

    @Override
    @NotNull
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        if (id.isEmpty())
            throw new IdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        repository.update(id, project);
        return project;
    }

}
